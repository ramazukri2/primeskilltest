using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroController : MonoBehaviour
{
    [SerializeField] LayerMask groundLayers;

    public float gravity = -20f;
    public CharacterController characterController;
    private Vector3 direction;
    public float speed = 8f;
    public float jumpForce = 10;
    public Transform groundCheck;
    public Transform model;
    public bool doubleJump = true;

    public Animator animator;
    public Toggle _toggle;
    public Rigidbody rb;

    //GenerateGround
    public GameObject[] section;
    public int xPos = 5;
    public bool creatingSection = false;
    public int secNum;

    //accelerator
    public float maxSpeed = 50f;
    public float timeZerotoMax = 2.5f;
    float accelRatePerSec;
    float forwardVelocity;

    //deccelator
    public float timeMaxToZero = 6f;
    public float timeBrakeToZero = 1f;
    float decelRatePerSec;
    float brakeRatePerSec;
 

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        accelRatePerSec = maxSpeed / timeZerotoMax;
        decelRatePerSec = -maxSpeed / timeMaxToZero;
        brakeRatePerSec = -maxSpeed / timeBrakeToZero;
        forwardVelocity = 0f;
    }

    void Update()
    {
        PlayerMove();
        if(creatingSection == false)
        {
            creatingSection = true;
            StartCoroutine(GenerateSection());
            
        }

        _toggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(_toggle);
        });

        //PanelMov();

    }

    void LateUpdate()
    {
        rb.velocity = transform.forward * forwardVelocity;
    }

    void ToggleValueChanged(Toggle tglValue)
    {
        if (_toggle.isOn)
        {
            forwardVelocity += accelRatePerSec * Time.deltaTime;
            forwardVelocity = Mathf.Min(forwardVelocity, maxSpeed);
        }else if (_toggle.isOn)
        {
            forwardVelocity += brakeRatePerSec * Time.deltaTime;
            forwardVelocity = Mathf.Max(forwardVelocity, 0);
        }
        else
        {
            characterController.Move(direction * Time.deltaTime);
        }
    }

    public void PlayerMove()
    {
        float hInput = Input.GetAxis("Horizontal");
        direction.x = hInput * speed;
        animator.SetFloat("speed", Mathf.Abs(hInput));
        bool isGrounded = Physics.CheckSphere(groundCheck.position, 0.15f, groundLayers);
        
        animator.SetBool("isGrounded", isGrounded);

        if (isGrounded)
        {
            direction.y = -1;
            doubleJump = true;
            if (Input.GetButtonDown("Jump"))
            {

                direction.y = jumpForce;
            }
        }
        else
        {
            direction.y += gravity * Time.deltaTime;
            if (doubleJump && Input.GetButtonDown("Jump"))
            {
                direction.y = jumpForce;
                doubleJump = false;
            }
            
        }

        if (hInput != 0)
        {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, 0));
            model.rotation = newRotation;
            ToggleValueChanged(_toggle);
        }
        characterController.Move(direction * Time.deltaTime);
    }

    IEnumerator GenerateSection()
    {
        if (characterController)
        {
            secNum = Random.Range(0, 2);
            Instantiate(section[secNum], new Vector3(xPos, 0, 0), Quaternion.identity);
            xPos += 5;
            yield return new WaitForSeconds(2);
            creatingSection = false;
        }
        
    }
}
